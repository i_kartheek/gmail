package gmail.java;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.Test;

public class multiplesessions {
	
	@Test   
    public void executeSessionOne()throws InterruptedException
	{
            //First session of WebDriver
		WebDriver driver = new FirefoxDriver();
		
            //Goto guru99 site
            driver.get("http://demo.guru99.com/V4/");
            
            Thread.sleep(10000);            
            //find user name text box and fill it
            driver.findElement(By.name("uid")).sendKeys("Driver 1");
             
        }
         
    @Test   
        public void executeSessionTwo()throws InterruptedException
      {
    	
    	System.setProperty("webdriver.chrome.driver","C:\\Users\\kartheek\\Desktop\\chromedriver.exe");
            //Second session of WebDriver
    	WebDriver driver = new ChromeDriver();
            //Goto guru99 site
        driver.get("http://demo.guru99.com/V4/");
        Thread.sleep(10000);
        //find user name text box and fill it
        driver.findElement(By.name("uid")).sendKeys("Driver 2");
         
        }
         
    /*@Test   
        public void executSessionThree()throws InterruptedException
    {
            //Third session of WebDriver
    	System.setProperty("webdriver.ie.driver","C:\\Users\\kartheek\\Desktop\\IEDriverServer.exe");
        WebDriver driver = new InternetExplorerDriver();
            //Goto guru99 site
        driver.get("http://demo.guru99.com/V4/");
        
        Thread.sleep(10000);
        //find user name text box and fill it
        driver.findElement(By.name("uid")).sendKeys("Driver 3");
         
        }        */

}
