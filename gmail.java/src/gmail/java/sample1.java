package gmail.java;

import java.awt.Robot;
import java.io.File;

import org.testng.Assert;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;


public class sample1 {
	
	
	@Test
	public void creation_account()
	{
		
		
		//url calling
		//init
		WebDriver driver=new FirefoxDriver();
		driver.get("http://www.gmail.com");
		
		
		
		//click on create 
		driver.findElement(By.xpath("//*[@id='link-signup']/a")).click();
		
		//verifying name text
		String s=driver.findElement(By.xpath("//*[@id='name-form-element']/fieldset/legend/strong")).getText();
		Assert.assertEquals(s,"Name");
		System.out.println("The text is:"+s);
		
		//sending names
		driver.findElement(By.id("FirstName")).sendKeys("kartheek1");
		driver.findElement(By.id("LastName")).sendKeys("itha");
		
		//choosing your username text
		String s1=driver.findElement(By.xpath("//*[@id='gmail-address-label']/strong")).getText();
		Assert.assertEquals(s1,"Choose your username");
		System.out.println("The text is:"+s1);
		
		//placeholder text not working
		 String placetext=driver.findElement(By.xpath("//*[@id='GmailAddress']")).getAttribute("name");
	     System.out.println("the place holder of choose your username is:"+placetext);
	     
	    
		
		driver.findElement(By.id("GmailAddress")).sendKeys("kartheek.820");
		driver.findElement(By.id("Passwd")).sendKeys("@k1234567");
		driver.findElement(By.id("PasswdAgain")).sendKeys("@k1234567");
		
		
		//passing birthdate
	     
		WebElement w = driver.findElement(By.xpath("//*[@id='BirthMonth']/div[1]"));
		
		Select dropdown = new Select(w);
		
		dropdown.selectByVisibleText("September");

		
		driver.findElement(By.xpath("//*[@id='BirthDay']")).sendKeys("26");
		driver.findElement(By.xpath("//*[@id='BirthYear']")).sendKeys("1991");
		driver.findElement(By.xpath("//*[@id='RecoveryPhoneNumber']")).sendKeys("8939061494");
		driver.findElement(By.xpath("//*[@id='RecoveryEmailAddress']")).sendKeys("kartheek.itha1@gmail.com");
		
		driver.findElement(By.xpath("//*[@id='SkipCaptcha']")).click();
		
		driver.findElement(By.xpath("//*[@id='TermsOfService']")).click();
		
		
		
		WebElement gender = driver.findElement(By.xpath("//*[@id='Gender']/div[1]"));
		
		Select dd1 = new Select(gender);
		
		dd1.selectByVisibleText("Male");
		
		driver.findElement(By.id("submitbutton")).click();
		
		
		
		
		
		
		
		
		
		
		//driver.findElement(By.id("PasswdAgain")).click();
		
		//String redtext_pwrd = driver.findElement(By.xpath("//*[@id='errormsg_0_GmailAddress']")).getText();
		//System.out.println("the red text is:"+redtext_pwrd);
	     
		
		
		
		
		
	}

	

	@Test
	
	public void signin() throws Exception
	{
		
		
	//url calling
	WebDriver driver=new FirefoxDriver();
	driver.get("http://www.gmail.com");
	
	String parentHandle = driver.getWindowHandle();
	
	
	driver.findElement(By.id("Email")).sendKeys("kartheek.itha1");
	driver.findElement(By.id("next")).click();
	Thread.sleep(600);
	
	driver.findElement(By.id("Passwd")).sendKeys("Kartheek@1234");
	//check box checking
	
	WebElement wc=driver.findElement(By.xpath("//*[@id='PersistentCookie']"));
	wc.click();
	wc.click();
	//Thread.sleep(2000);
	
	Assert.assertTrue(wc.isSelected());
	
	// stay signin_text check
	String s =driver.findElement(By.xpath("//*[@id='gaia_loginform']/div[2]/div/label/span")).getText();
	Assert.assertEquals(s,"Stay signed in");
	System.out.println("the text is:" +s);
	
	
	//forgot password text_check
	String s1=driver.findElement(By.xpath("//*[@id='link-forgot-passwd']")).getText();
	Assert.assertEquals(s1,"Forgot password?");
	System.out.println("the text at forgot link is:"+s1);
	
	
	Thread.sleep(2000);
	
	
	
	//forgot password link check
	//driver.findElement(By.linkText("Forgot password?")).click();
	
	driver.findElement(By.id("signIn")).click();
	Thread.sleep(6000);
	
	
	
	  //click on compose mail
		
		
		 driver.findElement(By.xpath("//*[@id=':jd']/div/div")).click();
	      Thread.sleep(10000);
	
	
	
	     driver.findElement(By.xpath("//*[@id=':pg']")).sendKeys("kartheek.itha1@gmail.com");
	     Thread.sleep(6000);
	
	     driver.findElement(By.xpath("//*[@id=':p1']")).sendKeys("Hello");
	     Thread.sleep(6000);
	     
	     
	     //click on send
	     driver.findElement(By.xpath("//*[@id=':or']")).click();
	     
	  //screenshot
	 File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	 FileUtils.copyFile(srcFile,new File("C:\\Users\\kartheek\\Desktop\\luck.jpeg"));
	     
	
	
	}
	
	
	
	
	
 }
	
	

