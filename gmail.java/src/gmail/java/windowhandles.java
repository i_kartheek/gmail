package gmail.java;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class windowhandles {
	
	WebDriver driver;
	static int l;
	
	
	@Test
	public static void handles() throws Exception
	{
		
		WebDriver driver= new FirefoxDriver();
		
		driver.get("http://www.durgajobs.com");
		
		String parentHandle = driver.getWindowHandle();
		
		System.out.println("parent handle adrs is"+parentHandle);
		
		driver.findElement(By.xpath("html/body/div[2]/table/tbody/tr/td/table[2]/tbody/tr[4]/td[7]/a")).click();//new window open
		
		
		Set<String> s1=driver.getWindowHandles();
		l=s1.size();
		System.out.println("no.of windows"+l);
		System.out.println("all windows are"+s1);
		
		
		/*for (String winHandle : driver.getWindowHandles()) {
		    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}*/

		Thread.sleep(6000);
		
		//driver.close();
		driver.switchTo().window(parentHandle);
		
		
		
	}

}

